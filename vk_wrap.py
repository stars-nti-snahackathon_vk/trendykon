import logging
import confit
from math import floor, ceil
import datetime
import dateutil.relativedelta
import vk_api

config = confit.LazyConfig('trendykon', __name__)

BATCH_PORTION_COUNT = config['BATCH_PORTION_COUNT'].get(int)

USER_FIELDS = config['USER_FIELDS'].get(list)

DEFAULT_BDATE = config['DEFAULT_BDATE'].get(str)

logger = logging.getLogger(__name__)

logger.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s - %(levelname)s: %(message)s',
                              datefmt="%H:%M:%S")

sh = logging.StreamHandler()
sh.setLevel(logging.DEBUG)
sh.setFormatter(formatter)

logger.addHandler(sh)

api = vk_api.VkApi(api_version=config['vk_api']['version'].get(float))
try:
    api.login = config['vk_api']['login'].get(str)
    api.password = config['vk_api']['password'].get(str)
    api.authorization()
except confit.NotFoundError:
    logger.warning('No login/pass for VK API specified')


def get_group_id(group_name: str) -> int:
    # group_name can either be a string (like 'meduzaproject') or int (like '76982440')

    group_info = api.method('groups.getById', {'group_id': group_name})[0]

    return group_info['id']


def get_group_members(group_id: str) -> list:

    count = api.method('groups.getMembers', {'group_id': group_id})['count']

    for batch_id in range(ceil(count / BATCH_PORTION_COUNT)):
        yield api.method('groups.getMembers',
                         {'group_id': group_id,
                          'offset': batch_id * BATCH_PORTION_COUNT})['items']


def get_wall_posts(owner_id,
                   count=100,
                   domain='',
                   offset=0,
                   filter_expr='all',
                   extended=False,
                   fields=None):
    # pylint: disable=too-many-arguments

    for i in range(floor(count / 100) + 1):
        logger.info('Retrieving posts %i-%i from %i', i * 100,
                    min(i * 100 + 100, count), count)
        posts = api.method('wall.get', {'owner_id': owner_id,
                                        'domain': domain,
                                        'offset': offset + i * 100,
                                        'count': min(100, count - i * 100),
                                        'filter': filter_expr,
                                        'extendend': int(extended),
                                        'fields': fields})

        yield posts['items']


def get_wall_posts_by_group(group_id, **kwargs) -> list:
    group_id = get_group_id(group_id)

    return get_wall_posts(-group_id, **kwargs)


def get_first_level_reposts(post, offset=0):
    """

    :param offset:
    :param post: Словарь, содержит как минимум owner_id и post_id
    :return:
    """

    if post.get('reposts', {'count': 0})['count'] == 0:
        return []

    logger.debug('Requesting...')
    first_part = api.method('wall.getReposts', {
        'owner_id': post.get('owner_id', post['from_id']),
        'post_id': post['id'],
        'count': BATCH_PORTION_COUNT,
        'offset': offset
    })['items']
    second_part = api.method('wall.getReposts', {
        'owner_id': post.get('owner_id', post['from_id']),
        'post_id': post['id'],
        'count': BATCH_PORTION_COUNT,
        'offset': offset + len(first_part)
    })['items']
    logger.debug('Done')
    return first_part + second_part


def get_all_reposts(post, level):

    if post.get('reposts', {'count': 0})['count'] == 0:
        post.update({'level': level})
        yield [post]

    reposts = get_first_level_reposts(post)

    for repost in reposts:
        yield get_all_reposts(repost, level + 1)


def get_user_data(user_ids: int, fields: list=USER_FIELDS) -> dict:
    user_data = api.method('users.get', {'user_ids': user_ids,
                                         'fields': ','.join(fields)})

    try:
        user_data = user_data[0]
    except IndexError:
        user_data = {}

    return user_data


def get_user_friends(user_id: int) -> list:

    count = api.method('friends.get', {'user_id': user_id})['count']

    for batch_id in range(ceil(count / BATCH_PORTION_COUNT)):
        logger.info('Retrieving friends batch %i-%i / %i',
                    batch_id * BATCH_PORTION_COUNT,
                    (batch_id + 1) * BATCH_PORTION_COUNT, count)
        yield api.method('friends.get',
                         {'user_id': user_id,
                          'offset': batch_id * BATCH_PORTION_COUNT})['items']


def _get_user_bdate_str(
        user_id: int,
        bdate_cache:
        'dict; a simple way to cache bdates in memory' =None) -> str:
    if bdate_cache:
        try:
            return bdate_cache[user_id]
        except KeyError:
            bdate_cache[user_id] = get_user_data(user_id, ['bdate']).get(
                'bdate', DEFAULT_BDATE)
            return bdate_cache[user_id]
    else:
        return get_user_data(user_id, ['bdate']).get('bdate', DEFAULT_BDATE)


def get_user_bdate(
        user_id: int,
        bdate_cache:
        'dict; a simple way to cache bdates in memory' =None) -> str:
    str_bdate = _get_user_bdate_str(user_id, bdate_cache)

    try:
        return datetime.datetime.strptime(str_bdate, '%d.%m.%Y').date()
    except ValueError:
        return datetime.datetime.strptime(DEFAULT_BDATE, '%d.%m.%Y').date()


def get_user_age(user_bdate:
                 datetime.date) -> dateutil.relativedelta.relativedelta:
    today = datetime.date.today()

    age = dateutil.relativedelta.relativedelta(today, user_bdate)

    return age


def find_users(**kwargs) -> list:

    count = kwargs['count'] or api.method('users.search', kwargs)['count']

    for batch_id in range(ceil(count / BATCH_PORTION_COUNT)):

        kwargs.update({'offset': batch_id * BATCH_PORTION_COUNT})

        yield [user['id']
               for user in api.method('users.search', kwargs)['items']]


def add_children(post, level=1):

    post['children'] = []

    for child in get_first_level_reposts(post):
        child.update({'level': level})
        post['children'].append(child)

    for child in post['children']:
        add_children(child, level + 1)


def get_tree(reposts):
    nodes = []
    edges = []

    for repost in reposts:
        parents = repost.get('copy_history')
        repost_id = '{}_{}'.format(repost['from_id'], repost['id'])
        nodes.append(repost_id)

        if parents:
            parent = parents[0]
            parent_id = '{}_{}'.format(parent['from_id'], parent['id'])
            edges.append((parent_id, repost_id))

    return nodes, edges


if __name__ == '__main__':
    for post_portion in get_wall_posts_by_group('meduzaproject', count=235):
        print(len(post_portion))

    post = api.method('wall.getById', {'posts': '-29534144_3308685'})[0]
    reposts = get_all_reposts(post, 0)
    for r in reposts:
        print(list(r['level']))
