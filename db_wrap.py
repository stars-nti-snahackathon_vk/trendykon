#!/usr/bin/python3

import pandas as pd


def get_reposters(post) -> list:
    reposts = post['reposts']
    from_ids = map(lambda repost: repost['from_id'], reposts)

    return pd.DataFrame([x for x in from_ids])
