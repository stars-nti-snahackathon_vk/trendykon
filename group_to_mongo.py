#!/usr/bin/python3

import argparse
import bson
import pymongo
import vk_wrap
import funcy
import concurrent.futures


def insert_post(post):
    reposts = [x for x in funcy.flatten(vk_wrap.get_all_reposts(post, 0))]

    post['reposts'] = reposts

    try:
        collection.insert_one(post)
    except pymongo.errors.DuplicateKeyError as e:
        print('DuplicateKeyError %s', e)


parser = argparse.ArgumentParser(
    description='Upload group\'s posts to MongoDB')
parser.add_argument('db_host', type=str)
parser.add_argument('group_name', type=str)
parser.add_argument('--user', type=str)
parser.add_argument('--password', type=str)
parser.add_argument('--post_count', type=int, default=200)
parser.add_argument('--post_offset', type=int, default=189)
parser.add_argument('--collection_name', type=str)
args = parser.parse_args()

co = bson.codec_options.CodecOptions(unicode_decode_error_handler='ignore')

cl = pymongo.MongoClient(args.db_host)
db = cl.get_database('vk', codec_options=co)
if args.user:
    db.authenticate(args.user, args.password)
collection = db[args.collection_name or args.group_name]

collection.create_index('id', unique=True)

with concurrent.futures.ThreadPoolExecutor() as executor:
    for post_portion in vk_wrap.get_wall_posts_by_group(
            args.group_name,
            count=args.post_count,
            offset=args.post_offset):
        executor.map(insert_post, post_portion)
