import csv
import logging
import concurrent.futures
from dateutil.relativedelta import relativedelta
from vk_wrap import *

# user_id: int -> user_bdate: str
bdate_cache = {}

logging.basicConfig(level=logging.INFO)


def get_user_target_weight(user_id: int, target_id: int) -> int:
    delta = abs(relativedelta(
        get_user_age(get_user_bdate(user_id, bdate_cache)), get_user_age(get_user_bdate(
            target_id, bdate_cache))).years)

    return 1 / (delta + 1)


nodes = {}
edges = []
csv.field_size_limit(500 * 1024 * 1024)


def work_row(row):
    logging.info('Working on row %s', row)
    edges.append({
        'Source': row['Source'],
        'Target': row['Target'],
        'Type': row['Type'],
        'Id': row['Id'],
        'Label': row['Label'],
        'Interval': row['Interval'],
        'Weight': get_user_target_weight(
            int(row['Source']), int(row[
                'Target']))
    })
    logging.debug('Updated row: %s', edges[-1])


with open('edges.csv') as csvfile, concurrent.futures.ThreadPoolExecutor() as executor:
    reader = csv.DictReader(csvfile)

    row_count = 0
    executor.map(work_row, reader)

with open('edges.1.csv', 'w') as csvfile:
    fieldnames = ['Source', 'Target', 'Type', 'Id', 'Label', 'Interval',
                  'Weight']
    writer = csv.DictWriter(csvfile, fieldnames=fieldnames)

    writer.writeheader()
    for edge in edges:
        writer.writerow(edge)
