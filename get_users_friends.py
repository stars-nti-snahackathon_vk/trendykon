import concurrent.futures
import csv
from vk_wrap import *


with concurrent.futures.ThreadPoolExecutor() as executor, open(
        'users_friends.1.csv', 'w') as f:

    writer = csv.DictWriter(f, fieldnames=['user', 'friends'])
    writer.writeheader()

    for batch in find_users(school=146, count=100):
        for user in batch:
            executor.map(
                writer.writerow({'user': user,
                                 'friends': [x for x in get_user_friends(user)
                                             ]}), batch)
